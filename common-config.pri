PROJECT_VERSION = 0.7

APPLICATION_NAME = "it.mardy.uploader"
# Use this function just to get the title extracted for translation
defineReplace(tr) { return($$1) }
APPLICATION_TITLE = $$tr("Flickr uploader")

INSTALL_DIR=$$(INSTALL_DIR)
CONFIG(qtc)|!isEmpty(INSTALL_DIR) {
    INSTALL_PREFIX = /
} else {
    INSTALL_PREFIX = $${TOP_BUILD_DIR}/click
}
CLICK_ARCH = $$system("dpkg-architecture -qDEB_HOST_ARCH")
