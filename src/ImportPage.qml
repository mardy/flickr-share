import Ubuntu.Content 1.3

ContentPeerPicker {
    id: root

    handler: ContentHandler.Source
    contentType: ContentType.Pictures

    onPeerSelected: {
        peer.selectionType = ContentTransfer.Multiple;
        peer.request();
        pageStack.pop()
    }

    onCancelPressed: pageStack.pop()
}
