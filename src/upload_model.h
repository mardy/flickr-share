/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UPLOADER_UPLOAD_MODEL_H
#define UPLOADER_UPLOAD_MODEL_H

#include <QAbstractListModel>
#include <QUrl>
#include <QVariant>

namespace Uploader {

class UploadModelPrivate;
class UploadModel: public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(bool busy READ isBusy NOTIFY isBusyChanged)
    Q_PROPERTY(int completedCount READ completedCount \
               NOTIFY completedCountChanged)
    Q_PROPERTY(QUrl organizerLink READ organizerLink \
               NOTIFY completedCountChanged)
    Q_PROPERTY(bool isPublic READ isPublic WRITE setPublic \
               NOTIFY isPublicChanged)
    Q_PROPERTY(bool isFriends READ isFriends WRITE setFriends \
               NOTIFY isFriendsChanged)
    Q_PROPERTY(bool isFamily READ isFamily WRITE setFamily \
               NOTIFY isFamilyChanged)
    Q_PROPERTY(bool isHidden READ isHidden WRITE setHidden \
               NOTIFY isHiddenChanged)
    Q_ENUMS(UploadStatus)

public:
    enum Roles {
        UrlRole = Qt::UserRole + 1,
        ProgressRole,
        StatusRole,
        TitleRole,
        DescriptionRole,
        TagsRole,
    };

    enum UploadStatus {
        Ready = 0,
        Uploading,
        Uploaded,
        Failed,
    };

    UploadModel(QObject *parent = 0);
    ~UploadModel();

    bool isBusy() const;
    int completedCount() const;
    QUrl organizerLink() const;

    void setPublic(bool value);
    bool isPublic() const;

    void setFriends(bool value);
    bool isFriends() const;

    void setFamily(bool value);
    bool isFamily() const;

    void setHidden(bool value);
    bool isHidden() const;

    Q_INVOKABLE void login(const QByteArray &consumerKey,
                           const QByteArray &consumerSecret,
                           const QByteArray &token,
                           const QByteArray &tokenSecret);
    Q_INVOKABLE void startUpload();
    Q_INVOKABLE void addPhoto(const QUrl &url);
    Q_INVOKABLE void clear();

    Q_INVOKABLE QVariant get(int row, const QString &role) const;
    Q_INVOKABLE void setTitle(int row, const QString &title);
    Q_INVOKABLE void setDescription(int row, const QString &description);
    Q_INVOKABLE void addTag(int row, const QString &tag);
    Q_INVOKABLE void removeTag(int row, const QString &tag);

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();
    void isBusyChanged();
    void completedCountChanged();
    void isPublicChanged();
    void isFriendsChanged();
    void isFamilyChanged();
    void isHiddenChanged();
    void finished();

private:
    UploadModelPrivate *d_ptr;
    Q_DECLARE_PRIVATE(UploadModel)
};

} // namespace

#endif // UPLOADER_UPLOAD_MODEL_H
