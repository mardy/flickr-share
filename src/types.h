/*
 * Copyright (C) 2014-2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_TYPES_H
#define IMAGINARIO_TYPES_H

#include <QDateTime>
#include <QList>
#include <QPointF>
#include <QString>
#include <QStringList>
#include <math.h>

namespace Imaginario {

#ifdef USE_DOUBLES_FOR_LATLON
typedef double Geo;
#else
typedef float Geo;
#endif

#ifdef USE_DOUBLES_FOR_LATLON
#define GSIN(x) sin(x)
#define GCOS(x) cos(x)
#define GASIN(x) asin(x)
#define GTAN(x) tan(x)
#define GATAN(x) atan(x)
#define GATAN2(x, y) atan2(x, y)
#define GEXP(x) exp(x)
#define GLOG(x) log(x)
#define GPOW(x, y) pow(x, y)
#define GSQTR(x) sqrt(x)
#define GFLOOR(x) floor(x)
#define GROUND(x) round(x)
#define GCEIL(x) ceil(x)
#define GTRUNC(x) trunc(x)
#define GMOD(x, y) fmod(x, y)
#else
#define GSIN(x) sinf(x)
#define GCOS(x) cosf(x)
#define GASIN(x) asinf(x)
#define GTAN(x) tanf(x)
#define GATAN(x) atanf(x)
#define GATAN2(x, y) atan2f(x, y)
#define GEXP(x) expf(x)
#define GLOG(x) logf(x)
#define GPOW(x, y) powf(x, y)
#define GSQTR(x) sqrtf(x)
#define GFLOOR(x) floorf(x)
#define GROUND(x) roundf(x)
#define GCEIL(x) ceilf(x)
#define GTRUNC(x) truncf(x)
#define GMOD(x, y) fmodf(x, y)
#endif

struct GeoPoint {
    GeoPoint(): lat(NAN), lon(0) {}
    GeoPoint(const QPointF &p): lat(p.x()), lon(p.y()) {}
    GeoPoint(Geo lat, Geo lon): lat(lat), lon(lon) {}
    Geo lat;
    Geo lon;

    inline GeoPoint normalized() const;
    QPointF toPointF() const { return QPointF(lat, lon); }
    Geo distanceTo(const GeoPoint &other) const;
    bool isValid() const { return lat == lat; /* NaN != NaN */ }
    friend inline bool operator==(const GeoPoint &, const GeoPoint &);
    friend inline bool operator!=(const GeoPoint &, const GeoPoint &);
};

GeoPoint GeoPoint::normalized() const
{
    GeoPoint ret(GMOD(lat, 180), GMOD(lon, 360));
    /* TODO: normalize latitude */
    if (ret.lon > 180) {
        ret.lon -= 360;
    } else if (ret.lon < -180) {
        ret.lon += 360;
    }
    return ret;
}

inline bool operator==(const GeoPoint &p1, const GeoPoint &p2) {
    return p1.lon == p2.lon && (p1.isValid() == p2.isValid()) &&
        (!p1.isValid() || p1.lat == p2.lat);
}

inline bool operator!=(const GeoPoint &p1, const GeoPoint &p2) {
    return p1.lon != p2.lon || (p1.isValid() != p2.isValid()) ||
        (p1.isValid() && p1.lat != p2.lat);
}

} // namespace

Q_DECLARE_METATYPE(Imaginario::GeoPoint)

#endif // IMAGINARIO_TYPES_H
