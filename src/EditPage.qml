import QtQuick 2.3
import Ubuntu.Components 1.3
import Ubuntu.Keyboard 0.1

Page {
    id: root

    property var modelData: null
    property var model: null
    property int index: -1

    header: PageHeader {
        title: i18n.tr("Edit metadata")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                id: goBack
                iconName: "back"
                onTriggered: {
                    root.updateModel()
                    pageStack.pop()
                }
            }
        ]
    }

    Flickable {
        id: flick
        anchors {
            left: parent.left; right: parent.right
            top: parent.top; bottom: keyboardRectangle.top
        }
        contentHeight: contentItem.childrenRect.height

        Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }
            spacing: units.gu(1)

            Column {
                anchors { left: parent.left; right: parent.right }
                Label {
                    anchors { left: parent.left; right: parent.right }
                    text: i18n.tr("Title")
                }
                TextField {
                    id: titleField
                    anchors { left: parent.left; right: parent.right }
                    text: modelData.title
                    placeholderText: i18n.tr("Enter a title")
                    KeyNavigation.tab: descriptionField
                    Keys.onReturnPressed: descriptionField.forceActiveFocus()
                }
            }

            Column {
                anchors { left: parent.left; right: parent.right }
                Label {
                    anchors { left: parent.left; right: parent.right }
                    text: i18n.tr("Description")
                }
                TextArea {
                    id: descriptionField
                    anchors { left: parent.left; right: parent.right }
                    text: modelData.description
                    autoSize: true
                    placeholderText: i18n.tr("Enter a description")
                    KeyNavigation.tab: tabField
                }
            }

            Column {
                anchors { left: parent.left; right: parent.right }
                Label {
                    anchors { left: parent.left; right: parent.right }
                    text: i18n.tr("Tags")
                }
                Flow {
                    id: flow
                    anchors { left: parent.left; right: parent.right }
                    spacing: units.gu(1)
                    Repeater {
                        model: modelData.tags
                        Rectangle {
                            width: contents.width + units.gu(1)
                            height: contents.height + units.gu(1)
                            color: "#eee"
                            radius: units.gu(1)

                            Item {
                                id: contents
                                anchors.centerIn: parent
                                width: childrenRect.width
                                height: childrenRect.height
                                Label {
                                    id: tagLabel
                                    width: Math.min(flow.width - units.gu(2), implicitWidth)
                                    verticalAlignment: Text.AlignVCenter
                                    text: modelData
                                    elide: Text.ElideRight
                                }
                                Image {
                                    anchors.left: tagLabel.right
                                    width: units.gu(1.5)
                                    height: width
                                    mipmap: true
                                    source: "qrc:/remove-tag"
                                }
                            }

                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    root.model.removeTag(root.index, modelData)
                                }
                            }
                        }
                    }
                }
                TextField {
                    id: tabField
                    anchors { left: parent.left; right: parent.right }
                    placeholderText: i18n.tr("Add tag")
                    inputMethodHints: Qt.ImhNoAutoUppercase
                    // TRANSLATORS: This is a key on the OSK, keep it short.
                    InputMethod.extensions: { "enterKeyText": i18n.ctr("OSK", "Add tag") }
                    Keys.onReturnPressed: {
                        root.model.addTag(root.index, text)
                        text = ""
                    }
                }
            }
        }
    }

    Item {
        id: keyboardRectangle
        anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
        height: Qt.inputMethod.keyboardRectangle.height
    }

    function updateModel() {
        model.setTitle(root.index, titleField.text)
        model.setDescription(root.index, descriptionField.text)
    }
}
