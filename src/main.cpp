/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QtQml>

#include "settings.h"
#include "upload_model.h"

static QObject *settingsProvider(QQmlEngine *, QJSEngine *)
{
    return new Uploader::Settings;
}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    app.setApplicationName(APPLICATION_NAME);

    qmlRegisterSingletonType<Uploader::Settings>("Flickr", 1, 0, "Settings",
                                                 settingsProvider);
    qmlRegisterType<Uploader::UploadModel>("Flickr", 1, 0, "UploadModel");

    QQuickView view;
    view.setSource(QUrl(QStringLiteral("qrc:///Main.qml")));
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.show();
    QObject::connect(view.engine(), &QQmlEngine::quit, &app, &QGuiApplication::quit);
    return app.exec();
}

